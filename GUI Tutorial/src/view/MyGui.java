package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class MyGui extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGui frame = new MyGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 507, 524);						
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnRed = new JButton("Rot");
		btnRed.setForeground(Color.BLACK);
		btnRed.setBackground(Color.WHITE);
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRed_clicked();
			}
		});
		btnRed.setBounds(22, 44, 100, 21);
		contentPane.add(btnRed);
		
		JButton btnGreen = new JButton("Gr\u00FCn");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGreen_clicked();
			}
		});
		btnGreen.setBounds(166, 44, 100, 21);
		contentPane.add(btnGreen);
		
		JButton btnBlue = new JButton("Blau");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonBlue_clicked();
			}
		});
		btnBlue.setBounds(305, 44, 100, 21);
		contentPane.add(btnBlue);
		
		JLabel lblNewLabel = new JLabel("Hier bitte Text eingeben");
		lblNewLabel.setBounds(22, 10, 383, 13);
		contentPane.add(lblNewLabel);
		
		JButton btnYellow = new JButton("Gelb");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonYellow_clicked();
			}
		});
		btnYellow.setBounds(22, 75, 100, 21);
		contentPane.add(btnYellow);
		
		JButton btnStandard = new JButton("Standardfarbe");
		btnStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStandardColor_clicked();
			}
		});
		btnStandard.setBounds(166, 75, 100, 21);
		contentPane.add(btnStandard);
		
		JButton btnChooseColor = new JButton("Farbe w\u00E4hlen");
		btnChooseColor.setBounds(305, 75, 100, 21);
		contentPane.add(btnChooseColor);
		
		JButton btnArial = new JButton("Arial");
		btnArial.setBounds(22, 117, 100, 21);
		contentPane.add(btnArial);
		
		JButton btnComicSans = new JButton("Comic Sans MS");
		btnComicSans.setBounds(166, 117, 100, 21);
		contentPane.add(btnComicSans);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.setBounds(305, 117, 100, 21);
		contentPane.add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(22, 148, 383, 19);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnWriteInLbl = new JButton("Ins Label schreiben");
		btnWriteInLbl.setBounds(22, 177, 178, 21);
		contentPane.add(btnWriteInLbl);
		
		JButton btnDelTextInLbl = new JButton("Text im Label l\u00F6schen");
		btnDelTextInLbl.setBounds(225, 177, 180, 21);
		contentPane.add(btnDelTextInLbl);
		
		JButton btnRedText = new JButton("Rot");
		btnRedText.setBounds(22, 208, 100, 21);
		contentPane.add(btnRedText);
		
		JButton btnBlueText = new JButton("Blau");
		btnBlueText.setBounds(166, 208, 100, 21);
		contentPane.add(btnBlueText);
		
		JButton btnBlackText = new JButton("Schwarz");
		btnBlackText.setBounds(305, 208, 100, 21);
		contentPane.add(btnBlackText);
		
		JButton btnIncreaseFonts = new JButton("+");
		btnIncreaseFonts.setBounds(22, 239, 178, 21);
		contentPane.add(btnIncreaseFonts);
		
		JButton btnDecreaseFonts = new JButton("-");
		btnDecreaseFonts.setBounds(225, 239, 180, 21);
		contentPane.add(btnDecreaseFonts);
		
		JButton btnTextLeft = new JButton("linksb\u00FCndig");
		btnTextLeft.setBounds(22, 282, 100, 21);
		contentPane.add(btnTextLeft);
		
		JButton btnTextCtr = new JButton("zentriert");
		btnTextCtr.setBounds(166, 282, 100, 21);
		contentPane.add(btnTextCtr);
		
		JButton btnTextRight = new JButton("rechtsb\u00FCndig");
		btnTextRight.setBounds(305, 282, 100, 21);
		contentPane.add(btnTextRight);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnExit.setBounds(22, 345, 383, 51);
		contentPane.add(btnExit);
	}

	protected void buttonStandardColor_clicked() {
		this.contentPane.setBackground(Color.MAGENTA);		// Sztandardfarbe ?????
		
	}

	protected void buttonYellow_clicked() {
		this.contentPane.setBackground(Color.YELLOW);
		
	}

	protected void buttonBlue_clicked() {
		this.contentPane.setBackground(Color.BLUE);
		
	}

	protected void buttonGreen_clicked() {
		this.contentPane.setBackground(Color.GREEN);
		
	}

	public void buttonRed_clicked() {
		this.contentPane.setBackground(Color.RED);
		
	}
}
